public class Bunny{
	public boolean isCute;
	public int numOfEars;
	public String favoriteFood;
	
	public void shouldHunt(){
		if (this.isCute){
			System.out.println("I should not hunt this bunny. it's too cute!");
		}
		else System.out.println("I should hunt this bunny. it's ugly!");
	}
	public void giveFood(){
		System.out.println("I gave the bunny some "+this.favoriteFood+".");
	}
}