import java.util.Scanner;
public class VirtualPetApp{
	
	public static void main(String[] args){
		
		Bunny[] bunColony= new Bunny[4];
		Scanner sc= new Scanner(System.in);
		for (int i=0;i<bunColony.length;i++){
			bunColony[i]= new Bunny();
			System.out.println("is bunny "+(i+1)+" cute? type 1 for yes and 0 for no");
			int playerChoice= Integer.parseInt(sc.nextLine());
			if (playerChoice==1){
				bunColony[i].isCute = true;
			}
			else bunColony[i].isCute = false;
			
			System.out.println("How many ears does it have?");
			bunColony[i].numOfEars = Integer.parseInt(sc.nextLine());
			
			System.out.println("What is the bunny's favorite food?");
			bunColony[i].favoriteFood = sc.nextLine();
		}
		Bunny lastBun= bunColony[3];
		System.out.println(lastBun.isCute+" "+lastBun.numOfEars+" "+ lastBun.favoriteFood+".");
		
		Bunny firstBun= bunColony[0];
		firstBun.shouldHunt();
		firstBun.giveFood();
	}
	
	
	
	
	
}